FROM ubuntu:latest
WORKDIR /app

ARG port=5000 
ENV PORT=$port

RUN export PORT=5000

RUN apt update -y && apt upgrade -y && apt install tzdata

RUN echo "Asia/Kolkata" | tee /etc/timezone
RUN dpkg-reconfigure --frontend noninteractive tzdata

RUN apt update -y && apt upgrade -y && apt install bash git nodejs npm -y
RUN apt install make python gcc g++ build-essential -y


COPY package.json /app
RUN npm install
COPY . /app

EXPOSE 5000


# daemon for cron jobs
RUN echo 'crond' > /boot.sh
# RUN echo 'crontab .openode.cron' >> /boot.sh

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)


# Bundle app source

CMD ["node","master.js"]
